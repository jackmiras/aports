# Contributor: Olliver Schinagl <oliver@schinagl.nl>
# Maintainer: Olliver Schinagl <oliver@schinagl.nl>
pkgname=stress-ng
pkgver=0.16.04
pkgrel=0
pkgdesc="stress-ng will stress test a computer system in various selectable ways"
url="https://github.com/ColinIanKing/stress-ng"
arch="all"
license="GPL-2.0-or-later"
options="!check" # tests are not portable
makedepends="
	judy-dev
	libaio-dev
	libbsd-dev
	linux-headers
	lksctp-tools-dev
	zlib-dev
	"
subpackages="$pkgname-doc $pkgname-bash-completion:bashcomp:noarch"
source="$pkgname-$pkgver.tar.gz::https://github.com/ColinIanKing/stress-ng/archive/refs/tags/V$pkgver.tar.gz"

build() {
	export CFLAGS="${CFLAGS/-Os/-O2}"
	make
}

package() {
	make DESTDIR="$pkgdir" \
		JOBDIR="/usr/share/doc/$pkgname/example-jobs/" install
}

bashcomp() {
	depends=""
	pkgdesc="Bash completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	amove usr/share/bash-completion/completions
}

sha512sums="
9c0ebcef697cb1fcf6f0836a8d0d88da0be20ed0683c35304e540bf040beff4152e231ba98c8a3ebe3c22bbde66a22ff8929397c35f7e070b9498ad1275e7a8d  stress-ng-0.16.04.tar.gz
"
