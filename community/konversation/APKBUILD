# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=konversation
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://konversation.kde.org/"
pkgdesc="A user-friendly and fully-featured IRC client"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kidletime-dev
	kio-dev
	kitemviews-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	phonon-dev
	qca-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	samurai
	solid-dev
	"
_repo_url="https://invent.kde.org/network/konversation.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/konversation-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f2dab6cc43b0d96340027dec1c801d3503275498e6e6c8e731a756b3400f17c9f82a714a2aa9fb6d0edfbbb8ac406dacf3845fc5fecf82b471a6978e9b08a27e  konversation-23.08.0.tar.xz
"
