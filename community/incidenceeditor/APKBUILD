# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=incidenceeditor
pkgver=23.08.0
pkgrel=0
pkgdesc="KDE PIM incidence editor"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	calendarsupport-dev
	eventviews-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kdiagram-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kldap-dev
	kmailtransport-dev
	kmime-dev
	libkdepim-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/incidenceeditor.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/incidenceeditor-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# akonadi-sqlite-incidencedatetimetest and akonadi-mysql-incidencedatetimetest require running DBus
	# ktimezonecomboboxtest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(akonadi-(sqlite|mysql)-incidencedatetime|ktimezonecombobox)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
7c702f5af1cdaca1cb357694ea1053cd2279c028257a0338bebdc40244436d037584316042cd089198f6a4f1133e6dddc145c7f3c12fdaf863f7eae37a5b2f4c  incidenceeditor-23.08.0.tar.xz
"
