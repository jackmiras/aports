# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-workspace
pkgver=5.27.7
pkgrel=2
pkgdesc="KDE Plasma Workspace"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-or-later AND GPL-2.0-or-later AND MIT AND LGPL-2.1-only AND LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-only"
depends="
	accountsservice
	fprintd
	kactivitymanagerd
	kded
	kinit
	kio-extras
	kirigami2
	kquickcharts
	kwin
	milou
	pipewire-session-manager
	plasma-integration
	qt5-qtquickcontrols
	qt5-qttools
	tzdata
	"
depends_dev="
	appstream-dev
	baloo-dev
	gpsd-dev
	iso-codes-dev
	kactivities-stats-dev
	kcmutils-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	kded-dev
	kdelibs4support-dev
	kdesu-dev
	kglobalaccel-dev
	kholidays-dev
	ki18n-dev
	kidletime-dev
	kitemmodels-dev
	kjsembed-dev
	knewstuff-dev
	knotifyconfig-dev
	kpackage-dev
	kpeople-dev
	krunner-dev
	kscreenlocker-dev
	ktexteditor-dev
	ktextwidgets-dev
	kuserfeedback-dev
	kwallet-dev
	kwayland-dev
	kwin-dev
	layer-shell-qt-dev
	libkexiv2-dev
	libkscreen-dev
	libksysguard-dev
	libqalculate-dev
	networkmanager-qt-dev
	phonon-dev
	plasma-framework-dev
	prison-dev
	wayland-protocols
	zlib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	libxtst-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dbg $pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang $pkgname-zsh-completion"
_repo_url="https://invent.kde.org/plasma/plasma-workspace.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-workspace-$pkgver.tar.xz
	0001-$pkgname-avoid-potential-crash.patch::https://invent.kde.org/plasma/plasma-workspace/-/commit/fc01a7f837d06ee9e92d02f13acb79c2b06e9e3c.patch
	"
replaces="plasma-desktop<5.24 breeze<5.22.90"

build() {
	# reduce size of debug syms
	CFLAGS="$CFLAGS -O2 -g1" CXXFLAGS="$CXXFLAGS -O2 -g1" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DPLASMA_WAYLAND_DEFAULT_SESSION=TRUE
	cmake --build build
}

check() {
	# nightcolortest requires running dbus
	# testdesktop, lookandfeel-kcmTest, test_kio_fonts, servicerunnertest systemtraymodeltest are broken
	# tst_triangleFilter requires plasma-workspace to be installed
	# locationsrunnertest requires a running Wayland environment
	local skipped_tests="("
	local tests="
		nightcolortest
		testdesktop
		lookandfeel-kcmTest
		test_kio_fonts
		servicerunnertest
		systemtraymodeltest
		tst_triangleFilter
		locationsrunnertest
		"
	case "$CARCH" in
		arm*|aarch64|ppc64le) tests="$tests calculatorrunnertest" ;;
	esac
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	xvfb-run ctest --test-dir build --output-on-failure -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
7359d087cb94280ed0c191b0328b8aa2ec42460a6eb057a06bae0de3abcfa8e3cd7c374b47a4b1d08b56fc292892bac4c0f501527574e2c799c3d4c87591892f  plasma-workspace-5.27.7.tar.xz
1baf444f6cd386d7f424dd9229d75b1d10018ce2806527af953a44004e3977e804823266cea69baf0b06f5073206e3afc048fb5e91a26386adbcb4a1272ff9a9  0001-plasma-workspace-avoid-potential-crash.patch
"
