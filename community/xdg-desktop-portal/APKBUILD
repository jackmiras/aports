# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=xdg-desktop-portal
pkgver=1.17.0
pkgrel=1
pkgdesc="Desktop integration portal"
url="https://github.com/flatpak/xdg-desktop-portal"
arch="all"
license="LGPL-2.1-or-later"
depends="bubblewrap cmd:fusermount3"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	flatpak
	flatpak-dev
	fontconfig-dev
	fuse3-dev
	geoclue-dev
	gettext-dev
	glib-dev
	json-glib-dev
	libportal-dev
	meson
	pipewire-dev
	xmlto
	"
checkdepends="py3-dbusmock"
subpackages="$pkgname-dev $pkgname-lang $pkgname-doc"
source="https://github.com/flatpak/xdg-desktop-portal/releases/download/$pkgver/xdg-desktop-portal-$pkgver.tar.xz"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dsystemd=disabled \
		. output
	meson compile -C output
}

check() {
	TEST_IN_CI=true meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	cd "$pkgdir"

	# We don't need this
	rm -rf usr/lib/systemd

	mkdir -p usr/lib
	mv usr/share/pkgconfig usr/lib/
}

sha512sums="
1c5e95664bef8b977bdfa1dca001ec7853cc30e8327dbfeee6a32490e5763950c5d10777ff456605c0f6706932318a06e82f51423ecf60f985b9bb00e043ef3d  xdg-desktop-portal-1.17.0.tar.xz
"
