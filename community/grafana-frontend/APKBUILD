# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>

# Make sure to keep this package in sync with grafana.

# Frontend files split from main grafana package.
# This is because grafana's build process is
# > build backend -> test backend -> build frontend -> test frontend
# while alpine doesn't allow easily interleaving those.
# Second problem is grafana doesn't support building on anything except linux/amd64,
# while alpine requires every build to be done natively.
# This is also a reason why we use prebuilt frontend archive.

pkgname=grafana-frontend
pkgver=10.1.1
pkgrel=0
pkgdesc="Open source, feature rich metrics dashboard and graph editor (frontend files)"
url="https://grafana.com"
arch="noarch"
license="AGPL-3.0-only"
options="!check" # We don't build frontend from sources.
source="$pkgname-$pkgver-bin.tar.gz::https://dl.grafana.com/oss/release/grafana-$pkgver.linux-amd64.tar.gz"
builddir="$srcdir/grafana-$pkgver"

package() {
	install -dm755 "$pkgdir/usr/share/grafana"
	cp -r "$builddir/plugins-bundled" "$builddir/public" "$pkgdir/usr/share/grafana/"
}

sha512sums="
f916257d56f71e3c619c8d62f121435ffbc044735cdfbfc0b357ec81cd8c619f53d5c886a43534aa21e3a821c39880e3e575a2a4061a85fb471fa6219120c734  grafana-frontend-10.1.1-bin.tar.gz
"
