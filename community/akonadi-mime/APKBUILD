# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadi-mime
pkgver=23.08.0
pkgrel=0
pkgdesc="Libraries and daemons to implement basic email handling"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by akonadi
# ppc64le blocked by qt5-qtwebengine -> kaccounts-integration
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	akonadi-dev>=$pkgver
	kcodecs-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	kitemmodels-dev
	kmime-dev
	kxmlgui-dev
	libxslt-dev
	qt5-qtbase-dev
	shared-mime-info
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/pim/akonadi-mime.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-mime-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# mailserializerplugintest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "mailserializerplugintest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
3c42c52f65067f1e7453d165048b41c07297c2aa609761c5c527c3e3c9f927feb4de1c9ef88ed65415e068b1923c8805d2c6168bda16102a29b8b93f31f29d01  akonadi-mime-23.08.0.tar.xz
"
