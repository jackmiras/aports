# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=krdc
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/internet/krdc/"
pkgdesc="Remote Desktop Client"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="freerdp"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	knotifications-dev
	knotifyconfig-dev
	knotifyconfig-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libssh-dev
	libvncserver-dev
	samurai
	"
_repo_url="https://invent.kde.org/network/krdc.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/krdc-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ede4488eb3c5ff456fa245888d6f52df05438fd000ee5157074dc2c47e29aa9d07299d1235391fcf2c804e78f740838900dd1643a6620f8ac130c218c29c4bc2  krdc-23.08.0.tar.xz
"
