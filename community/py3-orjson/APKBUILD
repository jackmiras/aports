# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-orjson
pkgver=3.9.5
pkgrel=0
pkgdesc="Fast, correct Python JSON library supporting dataclasses, datetimes, and numpy"
url="https://github.com/ijl/orjson"
arch="all"
license="Apache-2.0 AND MIT"
makedepends="
	cargo
	py3-gpep517
	py3-maturin
	python3-dev
	"
checkdepends="
	py3-dateutil
	py3-numpy
	py3-pytest
	py3-tz
	tzdata
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/ijl/orjson/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/orjson-$pkgver"
options="net"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--config-json '{"build-args": "--frozen"}' \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
5e47a3698f08dcb36e4f044427e0b8faed773cbe5f99d601ec0b0c0ec521199c5ce9119700cc5464ccaaad847fb3d85ff46159cde936dddaa7c4aeacc39d44e2  py3-orjson-3.9.5.tar.gz
"
