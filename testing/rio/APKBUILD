# Contributor: Nicolas Lorin <androw95220@gmail.com>
# Maintainer: Nicolas Lorin <androw95220@gmail.com>
pkgname=rio
pkgver=0.0.18
pkgrel=0
pkgdesc="A hardware-accelerated GPU terminal emulator powered by WebGPU, focusing to run in desktops and browsers."
url="https://raphamorim.io/rio"
arch="all !s390x"
license="MIT"
# !check: no tests available
# net: required to fetch rust dependencies
options="!check net"
depends="$pkgname-terminfo"
makedepends="cargo cargo-auditable cmake expat-dev freetype-dev fontconfig-dev libxcb-dev ncurses python3"
subpackages="$pkgname-terminfo"
source="$pkgname-$pkgver.tar.gz::https://github.com/raphamorim/rio/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --no-default-features --features=x11,wayland
	tic -e rio -x -o terminfo misc/rio.terminfo
}

package() {
	install -Dm755 target/release/rio "$pkgdir"/usr/bin/rio
	install -Dm644 terminfo/r/rio "$pkgdir"/usr/share/terminfo/r/rio
}

terminfo() {
	amove usr/share/terminfo
}

sha512sums="
3b3fd074d8f5fb90dc9f8c6922431facf873181bdc030f696bd6f6018bf0439f8d3ba8a03c1a22c51f57f5964accbe630e668b617467301f3059a5d69f8d7a56  rio-0.0.18.tar.gz
"
