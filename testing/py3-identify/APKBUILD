# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-identify
_pyname=identify
pkgver=2.5.28
pkgrel=0
pkgdesc="File identification library for Python"
url="https://github.com/pre-commit/identify"
arch="noarch"
license="MIT"
depends="py3-ukkonen"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="$_pyname-$pkgver.tar.gz::https://github.com/pre-commit/identify/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
8242b2d233ef3e7c2bff959b5d9b80b0c882f94595b5d514d5b3a2b1a93fdcc06ad6c039c01a708e2ca50cf9675e8c881db96f23b2cafcff5b18189800c37b27  identify-2.5.28.tar.gz
"
